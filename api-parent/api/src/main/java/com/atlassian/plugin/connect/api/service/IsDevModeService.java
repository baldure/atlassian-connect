package com.atlassian.plugin.connect.api.service;

public interface IsDevModeService
{
    public boolean isDevMode();
}
