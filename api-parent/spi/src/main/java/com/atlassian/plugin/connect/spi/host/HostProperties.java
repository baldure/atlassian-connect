package com.atlassian.plugin.connect.spi.host;

public interface HostProperties
{
    String getKey();
}
