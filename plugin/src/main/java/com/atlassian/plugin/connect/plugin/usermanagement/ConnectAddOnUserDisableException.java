package com.atlassian.plugin.connect.plugin.usermanagement;

public class ConnectAddOnUserDisableException extends Exception
{
    public ConnectAddOnUserDisableException(Exception cause)
    {
        super(cause);
    }
}
