package com.atlassian.plugin.connect.plugin.capabilities.provider;

import com.atlassian.plugin.connect.spi.module.provider.ConnectModuleProvider;

/**
 * @since 1.0
 */
public interface NullModuleProvider extends ConnectModuleProvider
{

}
