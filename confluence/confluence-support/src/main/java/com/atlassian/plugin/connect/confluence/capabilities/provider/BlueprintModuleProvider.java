package com.atlassian.plugin.connect.confluence.capabilities.provider;

import com.atlassian.plugin.connect.modules.beans.BlueprintModuleBean;
import com.atlassian.plugin.connect.spi.module.provider.ConnectModuleProvider;

public interface BlueprintModuleProvider extends ConnectModuleProvider<BlueprintModuleBean>
{

}
